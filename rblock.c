#include <linux/config.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/timer.h>
#include <linux/types.h>
#include <linux/fcntl.h>
#include <linux/hdreg.h>
#include <linux/kdev_t.h>
#include <linux/vmalloc.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/buffer_head.h>
#include <linux/bio.h>

MODULE_LICENSE("Dual BSD/GPL");

static int rblock_major;
module_param(rblock_major, int, 0);

static int hardsect_size = 512;		/* Sector size */
module_param(hardsect_size, int, 0);

static int nsectors = 1024;		/* Drive size */
module_param(nsectors, int, 0);

static int ndevices = 4;
module_param(ndevices, int, 0);

/*
 * The different "request modes" we can use.
 */
enum {
	RM_SIMPLE = 0,	/* The extra-simple request function */
	RM_FULL,	/* The full-blown version */
	RM_NOQUEUE,	/* Use make_request */
};
static int request_mode = RM_SIMPLE;
module_param(request_mode, int, 0);

/*
 * Minor number and partition management.
 */
#define RBLOCK_MINORS	16
#define MINOR_SHIFT	4

/*
 * We can tweak our hardware sector size, but the kernel talks to us
 * in terms of small sectors, always.
 */
#define KERNEL_SECTOR_SIZE	512

/*
 * After this much idle time, the driver will simulate a media change.
 */
#define INVALIDATE_DELAY	(30 * HZ)

struct rblock_dev {
	int size;			/* Device size in sectors */
	u8 *data;			/* The data array */
	short users;			/* How many users */
	short media_change;		/* Flag a media change? */
	spinlock_t lock;		/* For mutual exclusion */
	struct request_queue *queue;	/* The device request queue */
	struct gendisk *gd;		/* The gendisk structure */
	struct timer_list timer;	/* For simulated media changes */
};

static struct rblock_dev *devices;

static void rblock_transfer(struct rblock_dev *dev, unsigned long sector,
		unsigned long nsect, char *buffer, int write)
{
	unsigned long offset = sector * KERNEL_SECTOR_SIZE;
	unsigned long nbytes = nsect * KERNEL_SECTOR_SIZE;

	if ((offset + nbytes) > dev->size) {
		pr_notice("Beyond-end write (%ld %ld)\n", offset, nbytes);
		return;
	}
	if (write)
		memcpy(dev->data + offset, buffer, nbytes);
	else
		memcpy(buffer, dev->data + offset, nbytes);
}

/*
 * Simple request
 */
static void rblock_request(request_queue *q)
{
	struct request *req;

	while ((req = elv_next_request(q)) != NULL) {
		struct rblock_dev *dev = req->rq_disk->private_data;

		if (blk_fs_request(req)) {
			pr_notice("Skip request\n");
			end_request(req, 0);
			continue;
		}

		rblock_transfer(dev, req->sector, req->current_nr_sectors,
				req->buffer, rq_data_dir(req));
		end_request(req, 1);
	}
}

/*
 * Transfer a single BIO.
 */
static int rblock_xfer_bio(struct rblock_dev *dev, struct bio *bio)
{
	int i;
	struct bio_vec *bvec;
	sector_t sector = bio->bi_sector;

	bio_for_each_segment(bvec, bio, i) {
		char *buffer = __bio_kmap_atomic(bio, i, KM_USER0);
		rblock_transfer(dev, sector, bio_cur_sectors(bio)),
				buffer, bio_data_dir(bio) == WRITE);

		sector += bio_cur_sectors(bio);
		__bio_kunmap_atomic(bio, KM_USER0);
	}

	return 0;
}

/*
 * Transfer a full request.
 */
static int rblock_xfer_request(struct rblock_dev *dev, struct request *req)
{
	struct bio *bio;
	int nsect = 0;

	rq_for_each_bio(bio, req) {
		rblock_xfer_bio(dev, bio);
		nsect += bio->bi_size / KERNEL_SECTOR_SIZE;
	}

	return nsect;
}


/*
 * Smarter request function that "handles clustering".
 */
static void rblock_full_request(request_queue_t *q)
{
	struct request *req;
	int sectors_xferred;
	struct rblock_dev *dev = q->queuedata;

	while ((req = elv_next_request(q)) != NULL) {
			if (!blk_fs_request(req)) {
				pr_notice("Skip request\n");
				end_request(req, 0);
				continue;
			}
			sectors_xferred = rblock_xfer_request(dev, req);
			if (!end_that_request_first(req, 1, sectors_xferred)) {
				blkdev_dequeue_request(req);
				end_that_request_last(req);
			}
	}
}

/*
 * The direct make request version.
 */
static int rblock_make_request(request_queue_t *q, struct bio *bio)
{
	struct rblock_dev *dev = q->queuedata;
	int status;

	status = rblock_xfer_bio(dev, bio);
	bio_endio(bio, bio->bi_size, status);

	return 0;
}


/*
 * Open device
 */
static int rblock_open(struct inode *inode, struct file *filp)
{
	struct rblock_dev *dev = inode->i_bdev->bd_disk->private_data;

	del_timer_sync(&dev->timer);
	filp->private_data = dev;

	spin_lock(&dev->lock);
	if (!dev->users)
		check_disk_change(inode->i_bdev);
	dev->users++;
	spin_unlock(&dev->lock);

	return 0;
}

/*
 * Close device
 */
static int rblock_release(struct inode *inode, struct file *filp)
{
	struct rblock_dev *dev = inode->i_bdev->bd_disk->private_data;

	spin_lock(&dev->lock);
	dev->users--;

	if (!dev->users) {
		dev->timer.expires = jiffies + INVALIDATE_DELAY;
		add_timer(&dev->timer);
	}
	spin_unlock(&dev->lock);

	return 0;
}

/*
 * Look for a (simulated) media change.
 */
int rblock_media_changed(struct gendisk *gd)
{
	struct rblock_dev *dev = gd->private_data;

	return dev->media_change;
}

/*
 * Revalidate.  WE DO NOT TAKE THE LOCK HERE, for fear of deadlocking
 * with open.  That needs to be reevaluated.
 */
int rblock_revalidate(struct gendisk *gd)
{
	struct rblock_dev *dev = gd->private_data;

	if (dev->media_change) {
		dev->media_change = 0;
		memset(dev->data, 0, dev->size);
	}

	return 0;
}

/*
 * The "invalidate" function runs out of the device timer; it sets
 * a flag to simulate the removal of the media.
 */
void rblock_invalidate(unsigned long ldev)
{
	struct rblock_dev *dev = (struct rblock_dev *)ldev;

	spin_lock(&dev->lock);
	if (dev->users || !dev->data)
		pr_warn("rblock: timer sanity check failed\n");
	else
		dev->media_change = 1;

	spin_unlock(&dev->lock);
}

/*
 * Implementation ioctl() for driver
 */
int rblock_ioctl(struct inode *inode, struct file *flip,
		unsigned int cmd, unsigned long arg)
{
	long size;
	struct hd_geometry geo;
	struct rblock_dev *dev = flip->private_data;

	switch (cmd) {
	case HDIO_GETGEO:
	/*
	 * Get geometry: since we are a virtual device, we have to make
	 * up something plausible.  So we claim 16 sectors, four heads,
	 * and calculate the corresponding number of cylinders.  We set the
	 * start of data at sector four.
	 */
		size = dev->size * (hardsect_size / KERNEL_SECTOR_SIZE);

		geo.cylinders = (size & ~0x3f) >> 6;
		geo.heads = 4;
		geo.sectors = 16;
		geo.start = 4;

		if (copy_to_user((void __user *)arg, &geo, sizeof(geo)))
				return -EFAULT;
		return 0;
	}
	/*
	 * Unknown command
	 */
	return -ENOTTY
}

static const struct block_device_operations rblock_ops = {
	.owner			= THIS_MODULE,
	.open			= rblock_open,
	.release		= rblock_release,
	.media_changed		= rblock_media_changed,
	.revalidate_disk	= rblock_revalidate,
	.ioctl			= rblock_ioctl
};

static void setup_device(struct rblock_dev *dev, int which)
{
	/*
	 * Memory allocating
	 */
	memset(dev, 0, sizeof(struct rblock_dev));
	dev->size = nsectors * hardsect_size;
	dev->data = vmalloc(dev->size);

	if (dev->data == NULL) {
		pr_notice("vmalloc failure\n");
		return;
	}

	spin_lock_init(&dev->lock);

	/*
	 * The timer which "invalidates" the device.
	 */
	init_timer(&dev->timer);
	dev->timer.data = (unsigned long) dev;
	dev->timer.function = rblock_invalidate;

	/*
	 * The I/O queue, depending on whether we are using our own
	 * make_request function or not.
	 */
	switch (request_mode) {
	case RM_NOQUEUE:
		dev->queue = blk_alloc_queue(GFP_KERNEL);
		if (dev->queue == NULL)
				goto out_vfree;
		blk_queue_make_request(dev->queue, rblock_make_request);
		break;
	case RM_FULL:
		dev->queue = blk_init_queue(rblock_full_request, &dev->lock);
		if (dev->queue == NULL)
			goto out_vfree;
		break;
	default:
		pr_notice("Bad request mode %d, using simple\n",
			  request_mode);
	case RM_SIMPLE:
		dev->queue = blk_init_queue(rblock_request, &dev->lock);
		if (dev->queue == NULL)
			goto out_vfree;
		break;
	}

	blk_queue_hardsect_size(dev->queue, hardsect_size);
	dev->queue->queuedata = dev;

	/*
	 * Initialize gendisk structure
	 */
	dev->gd = alloc_disk(RBLOCK_MINORS);
	if (!dev->gd) {
		pr_notice("alloc_disk failure\n");
		goto out_vfree;
	}
	dev->gd->major = rblock_major;
	dev->gd->first_minor = which * RBLOCK_MINORS;
	dev->gd->fops = &rblock_ops;
	dev->gd->queue = dev->queue;
	dev->gd->private_data = dev;

	snprintf(dev->gd->disk_name, 32, "rblock%c", which + 'a');
	set_capacity(dev->gd, nsectors * (hardsect_size / KERNEL_SECTOR_SIZE));
	add_disk(dev->gd);

	return;

out_vfree:
	if (dev->data)
		vfree(dev->data);
}


static int __init rblock_init(void)
{
	int i;

	rblock_major = register_blkdev(rblock_major, "rblock");
	if (rblock_major <= 0) {
		pr_warn("rblock: unable to get major number\n");
		return -EBUSY;
	}

	devices = kmalloc(ndevices * sizeof(struct rblock_dev), GFP_KERNEL);
	if (devices == NULL) {
		unregister_blkdev(rblock_major, "sbd");
		return -ENOMEM;
	}

	for (i = 0; i < ndevices; ++i)
		setup_device(devices + i, i);

	return 0;
}

static void rblock_exit(void)
{
	int i;
	for (i = 0; i < ndevices; ++i) {
		struct rblock_dev *dev = devices + i;

		del_timer_sync(&dev->timer);

		if (dev->gd) {
			del_gendisk(dev->gd);
			put_disk(dev->gd);
		}
		if (dev->queue) {
			if (request_mode == RM_NOQUEUE)
				blk_put_queue(dev->queue);
			else
				blk_cleanup_queue(dev->queue);
		}
		if (dev->data)
			vfree(dev->data);
	}

	unregister_blkdev(rblock_major, "rblock");
	kfree(devices);
}

module_init(rblock_init);
module_exit(rblock_exit);
